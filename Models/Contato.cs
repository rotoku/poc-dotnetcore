using System;              
using System.ComponentModel.DataAnnotations;

namespace poc_dotnetcore.Models   
{
  public class Contato
  {
    public int codigo { get; set; }
    [Required]
    public string nome { get; set; }
    [Required]
    [StringLength(15)]
    public string telefone { get; set; }
    [Required]
    [DataType(DataType.Date)]
    public DateTime dataNascimento { get; set; }

    public Contato(int codigo, string nome, string telefone, DateTime dataNascimento)
    {
      this.codigo = codigo;
      this.nome = nome;
      this.telefone = telefone;
      this.dataNascimento = dataNascimento;
    }
    
  }	  
  
}
