﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using poc_dotnetcore.Models;

namespace poc_dotnetcore.Controllers
{
    
    [ApiController]
    [Route("api/v1/contatos")]
    public class ContatoController : ControllerBase
    {
        private static List<Contato> contatos = new List<Contato>();
        private static int codigo = 3;

        private readonly ILogger<ContatoController> _logger;

        public ContatoController(ILogger<ContatoController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public IActionResult Create([FromBody] Contato contato)
        {
            contato.codigo = codigo++;
            contatos.Add(contato);
            return CreatedAtAction(nameof(Retrieve), new { codigo = contato.codigo }, contato);
        }

        [HttpGet("{codigo}")]
        public IActionResult Retrieve(int codigo)
        {
            _logger.LogInformation("Método Ler Contato...");
            preencherContatos();
            Contato contato = contatos.FirstOrDefault(c => c.codigo == codigo);
            if(contato != null)
            {
                return Ok(contato);
            } else
            {
              return NotFound();  
            }
            
        }

        [HttpPut("{codigo}")]
        public IActionResult Update(int codigo, [FromBody] Contato contato)
        {
            return Ok();
        }

        [HttpDelete("{codigo}")]
        public IActionResult Delete(int codigo)
        {
            return NoContent();
        }

        [HttpGet]
        public IActionResult List()
        {
            _logger.LogInformation("Método Listar Contatos...");
            preencherContatos();
            return Ok(contatos);
        }

        private void preencherContatos()
        {
            if(contatos.Count == 0)
            {
                contatos.Add(new Contato(1, "Fulano", "(11) 99999-9999", new DateTime(1985,7,1)));
                contatos.Add(new Contato(2, "Beltrano", "(21) 99999-9999", new DateTime(1966,6,3)));
                contatos.Add(new Contato(3, "Sicrano", "(37) 99999-9999", new DateTime(1963,11,12)));
            }            
        }

    }
}
