# poc-dotnetcore

POC .NET Core

```
## versão
$ dotnet --version
5.0.403

## criar projeto
dotnet new webapi -n poc-dotnetcore

## Construir
dotnet build

## Rodar
dotnet run
```

dotnet add package Newtonsoft.Json

Microsoft.EntityFrameworkCore, vamos instalar na versão 5.0.5.
Microsoft.EntityFrameworkCore.Tools
MySQL.EntityFrameworkCore

dotnet add package Microsoft.EntityFrameworkCore --version=5.0.5


```
docker build -t poc-dotnetcore .
docker run -it --rm -p 5000:5000 -p 5001:5001 --name poc-dotnetcore-sample poc-dotnetcore
```